#pragma once
#include <iostream>
class Fraction
{
private:
	int numerator;
	unsigned denominator;
public:
	Fraction(int _num = 1, unsigned _denom = 1);
	Fraction(const Fraction& f);
	Fraction Add(Fraction second);
	Fraction Sub(Fraction second);
	Fraction Multi(Fraction second);
	Fraction Div(Fraction second);
	Fraction& operator=(const Fraction& f);
	void Reduction();
	Fraction& operator++();
	Fraction& operator++(int);
	Fraction& operator--();
	Fraction& operator--(int);
    
	bool operator>(const Fraction& f) const;
	bool operator>=(const Fraction& f) const;
	bool operator<(const Fraction& f) const;
	bool operator<=(const Fraction& f) const;
	bool operator==(const Fraction& f) const;
	bool operator!=(const Fraction& f) const;

	Fraction operator+(const Fraction& f) const;
	Fraction operator-(const Fraction& f) const;
	Fraction operator*(const Fraction& f) const;
	Fraction operator/(const Fraction& f) const;
	
	friend std::ostream& operator<<(std::ostream& out, const Fraction& f);
	friend std::istream& operator>>(std::istream& in, Fraction& f);
};

