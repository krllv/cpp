#pragma once
#include "Fraction.h"
class Complex
{
private:
	Fraction re;
	Fraction im;
public: 
	Complex(Fraction _re, Fraction _im);
	Complex(const Complex& c);
	Complex& operator=(const Complex& c);
	Complex operator+(const Complex& c) const;
	Complex operator-(const Complex& c) const;
	Complex operator*(const Complex& c) const;
	Complex operator/(const Complex& c) const;
	Complex& operator++();
	Complex& operator++(int);
	Complex& operator--();
	Complex& operator--(int);
	Complex& operator+=(const Complex& c);
	Complex& operator-=(const Complex& c);
	Complex& operator*=(const Complex& c);
	Complex& operator/=(const Complex& c);
	bool operator==(const Complex& c) const;
	bool operator!=(const Complex& c) const;
	friend std::ostream& operator<<(std::ostream& out, const Complex& c);
	friend std::istream& operator>>(std::istream& in, Complex& c);
};