#include "Fraction.h"
#include <assert.h>

Fraction::Fraction(int _num, unsigned _denom): numerator(_num), denominator(_denom)
{
	assert(denominator != 0);
}

Fraction Fraction::Add(Fraction second)
{
	int newNum = this->numerator * second.denominator + second.numerator * this->denominator;
	int newDen = second.denominator * this->denominator;
	return Fraction(newNum, newDen);
}

Fraction Fraction::Sub(Fraction second)
{
	int newNum =  this->numerator * second.denominator - second.numerator * this->denominator;
	int newDen = second.denominator * this->denominator;
	return Fraction(newNum, newDen);
}

Fraction Fraction::Multi(Fraction second)
{
	int newNum = this->numerator * second.numerator;
	int newDen = this->denominator * second.denominator;
	return Fraction(newNum, newDen);
}

Fraction Fraction::Div(Fraction second)
{
	int newNum = this->numerator * second.denominator;
	int newDen = this->denominator * second.numerator;
	return Fraction(newNum, newDen);
}

Fraction::Fraction(const Fraction& f)
{
	numerator = f.numerator;
	denominator = f.denominator;
}

Fraction& Fraction::operator=(const Fraction& f)
{
	numerator = f.numerator;
	denominator = f.denominator;
	return *this;
}

int Nod(int a, int b)
{
	while (b > 0) {
		int c = a % b;
		a = b;
		b = c;
	}
	return a;
}

void Fraction::Reduction()
{
	int nod = Nod(abs(numerator), denominator);
	if (nod != 1) {
		numerator /= nod;
		denominator /= nod;
	}
}

Fraction& Fraction::operator++()
{
	numerator += denominator;
	return *this;
}

Fraction& Fraction::operator++(int)
{
	numerator += denominator;
	return *this;
}

Fraction& Fraction::operator--()
{
	numerator -= denominator;
	return *this;
}

Fraction& Fraction::operator--(int)
{
	numerator -= denominator;
	return *this;
}


bool Fraction::operator>(const Fraction& f) const
{
	return numerator * f.denominator > f.numerator * denominator;
}

bool Fraction::operator>=(const Fraction& f) const
{
	return numerator * f.denominator >= f.numerator * denominator;
}

bool Fraction::operator<(const Fraction& f) const
{
	return numerator * f.denominator < f.numerator * denominator;
}

bool Fraction::operator<=(const Fraction& f) const
{
	return numerator * f.denominator <= f.numerator* denominator;
}

bool Fraction::operator==(const Fraction& f) const
{
	return (numerator == f.numerator) && (denominator == f.denominator);
}

bool Fraction::operator!=(const Fraction& f) const
{
	return (numerator != f.numerator) || (denominator != f.denominator);
}


Fraction Fraction::operator+(const Fraction& f) const
{
	int _num = this->numerator * f.denominator + this->denominator * f.numerator;
	int _denom = this->denominator * f.denominator;
	return Fraction(_num, _denom);
}

Fraction Fraction::operator-(const Fraction& f) const
{
	int _num = this->numerator * f.denominator - this->denominator * f.numerator;
	int _denom = this->denominator * f.denominator;
	return Fraction(_num, _denom);
}

Fraction Fraction::operator*(const Fraction& f) const
{
	int _num = this->numerator * f.numerator;
	int _denom = this->denominator * f.denominator;
	return Fraction(_num, _denom);
}

Fraction Fraction::operator/(const Fraction& f) const
{
	int _num = this->numerator * f.denominator;
	int _denom = this->denominator * f.numerator;
	return Fraction(_num, _denom);
}

std::ostream& operator<<(std::ostream& out, const Fraction& f)
{
	if (f.numerator == 0)
		return out << 0;
	if (f.denominator == 1)
		return out << f.numerator;
	out << f.numerator << "/" << f.denominator;
	return out;
}

std::istream& operator>>(std::istream& in, Fraction& f)
{
	char x;
	in >> f.numerator >> x;
	if (x != '/')
		in.putback(x);
	else
		in >> f.denominator;
	return in;
}

