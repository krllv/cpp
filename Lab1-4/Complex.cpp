#include "Complex.h"
#include "Fraction.h"

Complex::Complex(Fraction _re, Fraction _im): re(_re), im(_im){}

Complex::Complex(const Complex& c): re(c.re), im(c.im){}

Complex& Complex::operator=(const Complex& c)
{
	re = c.re;
	im = c.im;
	return *this;
}

Complex Complex::operator+(const Complex& c) const
{
	Fraction _re = re + c.re;
	_re.Reduction();
	Fraction _im = im + c.im;
	_im.Reduction();
	return Complex(_re, _im);
}

Complex Complex::operator-(const Complex& c) const
{
	return Complex(re - c.re, im - c.im);
}

Complex Complex::operator*(const Complex& c) const
{
	Fraction _re = re * c.re - im * c.im;
	_re.Reduction();
	Fraction _im = re * c.im + c.re * im;
	_im.Reduction();
	return Complex(_re, _im);
}

Complex Complex::operator/(const Complex& c) const
{
	Fraction denom = c.re * c.re + c.im * c.im;
	Fraction a = re * c.re + im * c.im;
	a = a / denom;
	a.Reduction();
	Fraction b = im * c.re - re * c.im;
	b = b / denom;
	b.Reduction();
	return Complex(a, b);
}

Complex& Complex::operator++()
{
	re++;
	return *this;
}

Complex& Complex::operator++(int)
{
	re++;
	return *this;
}

Complex& Complex::operator--()
{
	re--;
	return *this;
}

Complex& Complex::operator--(int)
{
	re--;
	return *this;
}

Complex& Complex::operator+=(const Complex& c)
{
	re = re + c.re;
	re.Reduction();
	im = im + c.im;
	im.Reduction();
	return *this;
}

Complex& Complex::operator-=(const Complex& c)
{
	re = re - c.re;
	re.Reduction();
	im = im - c.im;
	im.Reduction();
	return *this;
}

Complex& Complex::operator*=(const Complex& c)
{
	Fraction _re = re * c.re - im * c.im;
	_re.Reduction();
	Fraction _im = re * c.im + c.re * im;
	_im.Reduction();
	re = _re;
	im = _im;
	return *this;
}

Complex& Complex::operator/=(const Complex& c)
{
	Fraction denom = c.re * c.re + c.im * c.im;
	Fraction a = re * c.re + im * c.im;
	a = a / denom;
	a.Reduction();
	Fraction b = im * c.re - re * c.im;
	b = b / denom;
	b.Reduction();
	re = a;
	im = b;
	return *this;
}

bool Complex::operator==(const Complex& c) const
{
	return (re == c.re) && (im == c.im);
}

bool Complex::operator!=(const Complex& c) const
{
	return (re != c.re) || (im != c.im);
}

std::ostream& operator<<(std::ostream& out, const Complex& c)
{
	out << c.re << " + " << c.im << "i" << std::endl;
	return out;
}

std::istream& operator>>(std::istream& in, Complex& c)
{
	in >> c.re >> c.im;
	return in;
}