#include <iostream>
#include "Fraction.h"
#include "Complex.h"

using namespace std;

int main() {
	Fraction a = Fraction(5, 6);
	Fraction y = Fraction(2, 5);
	Fraction x = Fraction();
	cout << "Enter fraction x: ";
	cin >> x;
	cout << "x = " << x << endl;
	cout << "y = " << y << endl;
	Fraction add = x + y;
	add.Reduction();
	cout << "x + y = " << add << endl;
	Fraction sub = x - y;
	sub.Reduction();
	cout << "x - y = " << sub << endl;
	Fraction mul = x * y;
	mul.Reduction();
	cout << "x * y = " << mul << endl;
	Fraction div = x / y;
	div.Reduction();
	cout << "x / y = " << div << endl;	

	Complex z = Complex(x, y);
	cout << "z = " << z;
	Complex c = Complex(z);
	Complex r = Complex(x, a);
	cout << "r = " << r;
	Complex Add = z + r;
	cout << "z + r = " << Add;
	Complex Sub = z - r;
	cout << "z - r = " << Sub;
	Complex Mul = z * r;
	cout << "z * r = " << Mul;
	Complex Div = z / r;
	cout << "z / r = " << Div;
	
	cout << "Enter complex c: ";
	cin >> c;
	cout << "c = " << c;
	cout << "z = " << z;
	if (z != c)
		cout << "z != c" << endl;
	if (z == c)
		cout << "z == c" << endl;
	++c;
	cout << "++c = " << c;
	c++;
	cout << "c++ = " << c;
	--c;
	cout << "--c = " << c;
	c--;
	cout << "c-- = " << c;
	cout << "z = " << z;
	z += c;
	cout << "z += c: " << z;
	z -= c;
	cout << "z -= c: " << z;
	
	Complex A = Complex(x, a);
	Complex B = Complex(A);
	cout << "A = " << A;
	A *= c;
	cout << "A *= c: " << A;
	cout << "B = " << B;
	B /= c;
	cout << "B /= c: " << B;
	return 0;
}